package tn.itbs.gestionProjet.openfeign;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import tn.itbs.gestionProjet.entities.Utilisateur;

@FeignClient("microservice-utilisateur")
public interface userFeign {
	
	
	@GetMapping(path = "/user/get/{id}")
	Optional<Utilisateur> findUtilisateur(@PathVariable("id") int id);

	
}