package tn.itbs.gestionProjet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.itbs.gestionProjet.entities.Tache;

@Repository
public interface TacheRepository extends JpaRepository<Tache, Integer>{

}
