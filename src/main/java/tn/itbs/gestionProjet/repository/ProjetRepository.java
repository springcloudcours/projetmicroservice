package tn.itbs.gestionProjet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.itbs.gestionProjet.entities.Projet;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Integer>{

}
