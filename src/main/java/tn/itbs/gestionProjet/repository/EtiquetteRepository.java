package tn.itbs.gestionProjet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.itbs.gestionProjet.entities.Etiquette;

@Repository
public interface EtiquetteRepository extends JpaRepository<Etiquette, Integer>{

}
